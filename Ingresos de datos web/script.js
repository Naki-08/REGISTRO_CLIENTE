document.getElementById("clienteForm").addEventListener("submit", function(event) {
    event.preventDefault();

    var nombre = document.getElementById("nombre").value;
    var email = document.getElementById("email").value;
    var telefono = document.getElementById("telefono").value;

    // Crear una nueva fila en la tabla con los datos del cliente
    var table = document.getElementById("clientesTable").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.rows.length);
    var cell1 = newRow.insertCell(0);
    var cell2 = newRow.insertCell(1);
    var cell3 = newRow.insertCell(2);
    cell1.innerHTML = nombre;
    cell2.innerHTML = email;
    cell3.innerHTML = telefono;

    // Limpiar el formulario después de enviar los datos
    document.getElementById("clienteForm").reset();
});

